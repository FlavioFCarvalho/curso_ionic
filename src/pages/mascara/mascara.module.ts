import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MascaraPage } from './mascara';
import { BrMaskerModule } from 'brmasker-ionic-3';

@NgModule({
  declarations: [
    MascaraPage,
  ],
  imports: [
    IonicPageModule.forChild(MascaraPage),
    BrMaskerModule
  ],
})
export class MascaraPageModule {}
